package Demo2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class App {

	public static void main(String[] args) {

		File file = new File("test.txt");

		// try catch block
		try {
			FileReader fr = new FileReader(file);
			
			System.out.println("Continuing");
		} catch (FileNotFoundException e) {
		System.out.println("file not found: " + file.toString());
			// to jest deafault
			// e.printStackTrace();
		}

		System.out.println("Finished");
	}

}
